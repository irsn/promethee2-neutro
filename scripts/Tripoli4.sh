#!/bin/bash

# @(#) tripoli4

print_usage_and_exit()
{	cat <<EOFusage
        usage : $0 donnees [donnees2 [...]]
                ou
        usage : $0 [-D] [-s NJOY|ENDL] [-c jeff3|endl] donnees [donnees2 [...]]
                -D : to print debug information durint T4 calculation
EOFusage
    exit 2
}

#------------
# Configure
#------------

# Default value of the options
#-----------------------------
SOPTION=NJOY
COPTION=t4path.ceav5
DOPTION=""

# Paths
# -----
TMP=/tmp
TRIPOLI_HOME=/opt/CRISTAL_V2/TRIPOLI-4-8.1
ENVPATH=$TRIPOLI_HOME/Env

# Path to the executable
# ----------------------
TYPE_MACH="LINUX"
export TYPE_MACH
archdir=""
# Path to the executable
# ----------------------
archdir="linux-intel" ; # 32 bits
uname -m | grep -q -E "(x86_64|amd64|i64)" && archdir="linux-intel-64" # 64 bits
# add compiler runtimes shared libraries
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$TRIPOLI_HOME/CODE/lib/${archdir}  
PROG=$TRIPOLI_HOME/CODE/bin/$archdir/static_tripoli4


# Tripoli ignores SIGTERM so we escalate the SIGTERM into a SIGKILL
trap "kill -SIGKILL 0" SIGTERM

#------------
# Get options
#------------

while getopts s:c:D Opt
do
    case $Opt in
        s)    SOPTION=$OPTARG ;;
        c)    COPTION=$OPTARG ;;
        D)    DOPTION="-D";; 
        *)   print_usage_and_exit ;;
    esac
done
shift $(( $OPTIND - 1 ))
[[ $# == 0 ]] && print_usage_and_exit

# Check and translate SOPTION
[[ "$SOPTION" != "TABPROB" ]] && SOPTION="NJOY"

# Check and translate COPTION
case $COPTION in 
    "endl" ) COPTION="pt4path.endl";;
    "jeff3") COPTION="t4path.ceav5";;
    "pt4path.endl") : ;;
    "t4path.ceav5") : ;;
    *) echo "option dictionnaire inconnue :" $COPTION ;
	print_usage_and_exit ;;
esac

#--------
# Execute
#--------

while [ $# != 0 ]
do
    FICH=$(basename $1)
    tr -d '\r' < $FICH > $FICH.unix
    listing=$FICH.t4.8.1.listing

    GRAF=`grep GRAF $FICH.unix | wc -l`
    echo "GRAF="$GRAF
    if [ ! "$GRAF" == "0" ]; then
        for i in `seq 1 10`; do if [ ! -e /tmp/.X$i-lock ]; then NDISPLAY=$i; break; fi done
        Xvfb :$NDISPLAY &
        PID_XVFB=$!
        echo $PID_XVFB >> PID
        echo "export DISPLAY=:"$NDISPLAY".0"
        export DISPLAY=:$NDISPLAY".0"
    fi

    nice -5 $PROG $DOPTION -d $FICH.unix -s $SOPTION -c $ENVPATH/$COPTION -o $listing &
    PID_TRIPOLI=$!
    echo $PID_TRIPOLI >> PID
    if [ ! "$GRAF" == "0" ]; then
	    while [ ! -e $listing ]; do echo "."; sleep 1; done
        PRINTED_GRAF=`grep "checking association" $listing`
        PRINTED_ERROR=`grep "error" $FICH.unix.out``grep "entry not found" $FICH.unix.out``grep ERROR $listing`
        while [ "$PRINTED_GRAF" == "" ] && [ "$PRINTED_ERROR" == "" ]; do
            sleep 3
            echo "Return"
            xdotool key Return
            PRINTED_GRAF=`grep "checking association" $listing`
            PRINTED_ERROR=`grep "error" $FICH.unix.out``grep "entry not found" $FICH.unix.out``grep ERROR $listing`
        done
        kill -2 $PID_XVFB
        rm -f /tmp/.X$NDISPLAY-lock
    fi
    wait $PID_TRIPOLI
    echo "Machine d'execution : " `hostname` >> $listing

    rm -f PID

    PRINTED_ERROR=`grep "error" $FICH.unix.out``grep "entry not found" $FICH.unix.out``grep ERROR $listing`
    if [ ! "$PRINTED_ERROR" == "" ]; then
		echo "Calculation failed. exiting...."
		echo $PRINTED_ERROR >&2
		#echo " simulation time -1" >> $listing # turn around no longer needed in 1.1
    	exit 10
    fi

    shift
done

rm -f *.unix
