ace.define("ace/mode/moret_highlight_rules",["require","exports","module","ace/lib/oop","ace/lib/lang","ace/mode/text_highlight_rules"],function(require, exports, module) {
"use strict";

var oop = require("../lib/oop");
var lang = require("../lib/lang");
var TextHighlightRules = require("./text_highlight_rules").TextHighlightRules;

var moretHighlightRules = function() {

    var keywords = lang.arrayToMap(
            ("MORET_BEGIN|DEBUT_moret|FIN_moret|GEOM|CHIM|ASSO|SORT|SOUR|SIMU|ARRE|ESTI|COMB|TEST|IMPR|FINE|FEST|ALBE|GRAP|PEIN|NOTG|STOP|MESS|FIND|MODU|FING|VOLU|TROU|TYPE|RPLA|RHEX|RBOI|RCYL|RSPH|RELL|RESC|RESH|FINM|BOIT|LBOI|SPHE|CYLX|CYLY|CYLZ|ELLI|CYLQ|CONX|CONY|CONZ|PPLA|HEXX|HEXY|HEXZ|PLAX|PLAY|PLAZ|OBLI|ROTA|ROTA|DIAM|ROTA|ANGL|ROTA|SUPE|SUP|INFE|INF|ROTA|X|Y|Z|MPRI|DIMR|INDP|MSEC|NAPP|ENLB|BORD|FACE|ENLM|GARD|FINR|FRES|REVX|REVY|REVZ|ECRA|EECR|REUN|INTE|ETSU|DLIM|CENT|DIST|COLA|AZIM|PONC|MULT|SEAL|MACR|MICR|DISK|AP2H|ANIS|IMPR|FINC|FCHI|APO1|APO2|ANIS|IMPR|DANI|FINC|FCHI|CONC|VERI|PROP|BIBL|TEMP|FORM|GRUN|COMP|FINC|FCHI|FINE|FEXC|BIBL|TEMP|FORM|CONC|LIST|COND|PROM|PROA|FINC|FCOM|FICH|AP1H|APO1|APO2|DRAG|SCAL|FINF|FFIC|COMP|AP1H|APO1|APO2|DRAG|SCAL|IMPR|DANI|FINC|FCHI|ANIS|CONC|COND|PROM|PROA|PROV|FINC|FCOM|NORD|NOMC|FICH|ANIS|ISOT|DIRA|COVE|COCO|LUXT|QUAD|PERT|ISOU|MILI|REMP|CREF|VARD|DREF|FINA|FASS|REPO|LIST|SOMM|COEF|PROD|FINR|FREP|DECO|DEFI|GREL|LIST|FIND|FDEC|LIEU|DEFI|LIST|SOMM|FINL|FLIE|LIMI|LIST|SOMM|FINL|FLIM|SYST|LIST|FINS|FSYS|ESTI|LIST|FINE|FEST|CIBL|LIST|SOMM|FINC|FCIB|SCOR|ACTI|SEUL|EXCE|TOUS|ANNU|SEUL|EXCE|TOUS|DEFI|MODI|FINS|FSCO|SUPP|KEFF|ERR1|PSUP|PRIO|SHAN|MAIL|ERR1|PSUP|PRIO|FINS|FSUP|POST|SCOR|STAR|FINP|FPOS|CVOL|PAS|COUR|SREF|CINE|XMLC|SKIP|FINS|FSOR|REPO|NORM|LETH|DECO|LIEU|LIMI|SYST|ESTI|CIBL|NOYS|POIN|UNIF|FINP|FPOI|FINU|FUNI|FINS|FSOU|NATU|STRA|SUPH|MKIJ|IMPO|SURE|WIEL|TXCV|PARA|FISS|DESC|NGEN|STRA|FRNN|PROD|FRVP|FVPA|SURE|WIEL|TXCV|KINI|MODF|MATI|MATR|DENF|ABSI|EXPT|ROUL|ALEA|WOOD|SAUV|FINS|FSIM|APPR|MAIL|SAUV|FINA|FAPP|ENSE|REST|FINE|FENS|MODU|MMOD|SEUL|FSEU|EXCE|FINE|FEXC|VOLU|MVOL|TROU|MTRO|MAIL|MMAI|ETAP|ACTI|PASS|NEUT|ACTI|PASS|KEFF|SIGM|DIFF|INFE|SUPE|FINA|FARR|X|Y|Z|COUL|SVOL|NOMV|NOMM|ZOOM|FING|FGRA|FINP|FPEI")
                  .split("|")
            );

    var escapeRe = /\\u[0-9a-fA-F]{4}|\\/;

    var decimalInteger = "(?:(?:[1-9]\\d*)|(?:0))";
    var integer =  decimalInteger ;

    var exponent = "(?:[eE][+-]?\\d+)";
    var fraction = "(?:\\.\\d+)";
    var intPart = "(?:\\d+)";
    var pointFloat = "(?:(?:" + intPart + "?" + fraction + ")|(?:" + intPart + "\\.))";
    var exponentFloat = "(?:(?:" + pointFloat + "|" +  intPart + ")" + exponent + ")";
    var floatNumber = "(?:" + exponentFloat + "|" + pointFloat + ")";
    
    this.$rules = {
        "start" : [
            {
            	token : "parameter.comment",
            	regex : "^\\*\\@:.*$"
        	}, {
            	token : "parameter.formula",
            	regex : '\\@\\{.*\\}'
        	}, {
            	token : "parameter.variable",
            	regex : '\\$\\(.*\\)'
        	}, {
            	token : "parameter.variable.simple",
            	regex : '\\$\\S+'
            }, {
                token : "comment",
                regex : "\\*.*\$"
            }, {
                token : "constant.language.escape",
                regex : escapeRe
            }, {
                token : "constant.numeric", // float
                regex : floatNumber
            }, {
                token : "constant.numeric", // integer
                regex : integer + "\\b"
            }, {
               onMatch : function(value) {
                  if (keywords[value.substring(0,4).toUpperCase()])
                     return "keyword";
                  else
                     return "identifier";
               },
               regex : "[a-zA-Z.][a-zA-Z0-9._]*\\b"
            }, {
                defaultToken: "string"
            }
        ]
    };

};

oop.inherits(moretHighlightRules, TextHighlightRules);

exports.moretHighlightRules = moretHighlightRules;
})

,ace.define("ace/mode/moret",["require","exports","module","ace/lib/oop","ace/lib/lang","ace/mode/text","ace/mode/moret_highlight_rules"],function(require,exports,module){
	"use strict";
	var oop=require("../lib/oop");
   	var TextMode = require("./text").Mode;
   	var moretHighlightRules = require("./moret_highlight_rules").moretHighlightRules;
   	
	var Mode = function(){
      this.HighlightRules = moretHighlightRules;
   };
   oop.inherits(Mode, TextMode);
     
   (function() {
      this.$id = "ace/mode/moret";
   }).call(Mode.prototype);
   exports.Mode = Mode;
});                (function() {
                    ace.require(["ace/mode/moret"], function(m) {
                        if (typeof module == "object" && typeof exports == "object" && module) {
                            module.exports = m;
                        }
                    });
                })();
