ace.define("ace/mode/scale_highlight_rules",["require","exports","module","ace/lib/oop","ace/lib/lang","ace/mode/text_highlight_rules"], function(require, exports, module) {
"use strict";

var oop = require("../lib/oop");
var lang = require("../lib/lang");
var TextHighlightRules = require("./text_highlight_rules").TextHighlightRules;

var scaleHighlightRules = function() {

    var keywords = lang.arrayToMap(
            ("READ|GEOMETRY|COMP|END|GLOBAL|MEDIA|SPHERE|CUBOID|BOUNDARY")
                  .split("|")
            );

    var escapeRe = /\\u[0-9a-fA-F]{4}|\\/;

    var decimalInteger = "(?:(?:[1-9]\\d*)|(?:0))";
    var integer =  decimalInteger ;

    var exponent = "(?:[eE][+-]?\\d+)";
    var fraction = "(?:\\.\\d+)";
    var intPart = "(?:\\d+)";
    var pointFloat = "(?:(?:" + intPart + "?" + fraction + ")|(?:" + intPart + "\\.))";
    var exponentFloat = "(?:(?:" + pointFloat + "|" +  intPart + ")" + exponent + ")";
    var floatNumber = "(?:" + exponentFloat + "|" + pointFloat + ")";
    
    this.$rules = {
        "start" : [
            {
            	token : "parameter.comment",
            	regex : "^\\'\\@:.*$"
            }, {
            	token : "parameter.formula",
            	regex : '\\@\\{.*\\}'
        	},{
        		token : "parameter.variable",
        		regex : '\\&\\(.*\\)'
			},{
				token : "parameter.variable.simple",
				regex : '\\&\\S+'
			}, {
                token : "comment",
                regex : "\\'.*\$"
            }, {
                token : "constant.language.escape",
                regex : escapeRe
            }, {
                token : "constant.numeric", // float
                regex : floatNumber
            }, {
                token : "constant.numeric", // integer
                regex : integer + "\\b"
            }, {
               onMatch : function(value) {
                  if (keywords[value.toUpperCase()])
                     return "keyword";
                  else
                     return "identifier";
               },
               regex : "[a-zA-Z.][a-zA-Z0-9._]*\\b"
            }, {
                defaultToken: "string"
            }
        ]
    };

};

oop.inherits(scaleHighlightRules, TextHighlightRules);

exports.scaleHighlightRules = scaleHighlightRules;
})

,ace.define("ace/mode/scale",["require","exports","module","ace/lib/oop","ace/lib/lang","ace/mode/text","ace/mode/scale_highlight_rules"],function(require,exports,module){
	"use strict";
	var oop=require("../lib/oop");
   	var TextMode = require("./text").Mode;
   	var scaleHighlightRules = require("./scale_highlight_rules").scaleHighlightRules;
   	
	var Mode = function(){
      this.HighlightRules = scaleHighlightRules;
      this.$behaviour = this.$defaultBehaviour;
   };
   oop.inherits(Mode, TextMode);
     
   (function() {
      this.$id = "ace/mode/scale";
   }).call(Mode.prototype);
   exports.Mode = Mode;
});                (function() {
                    ace.require(["ace/mode/scale"], function(m) {
                        if (typeof module == "object" && typeof exports == "object" && module) {
                            module.exports = m;
                        }
                    });
                })();
