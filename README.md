# Promethee2-*Neutro*

This is the repository for distribution of Promethee for neutronics studies. It contains Windows, Linux, OSX packages with:

* Code plugins:
  * MCNP
  * Scale
  * CRISTAL
  * Moret
* Syntax support:
  * MCNP
  * Scale
  * Moret
* Algorithms:
  * Brent root finding
  * Gradient descent
  * Efficient Global Optimization (EGO)
  * Stepwise Uncertainty Reduction (SUR)
  * Robust Stepwise Uncertainty Reduction (RSUR)

---

Downloads:

* [Windows](https://gitlab.com/irsn/promethee2-neutro/-/jobs/artifacts/master/raw/packages/win64b/promethee2-neutro_win64b.zip?job=dist)
* [Linux](https://gitlab.com/irsn/promethee2-neutro/-/jobs/artifacts/master/raw/packages/linux64b/promethee2-neutro_linux64b.zip?job=dist)
* [OSX](https://gitlab.com/irsn/promethee2-neutro/-/jobs/artifacts/master/raw/packages/osx64b/promethee2-neutro_osx64b.zip?job=dist)

![Analytics](https://ga-beacon.appspot.com/UA-109580-11/Promethee2-Neutro)
